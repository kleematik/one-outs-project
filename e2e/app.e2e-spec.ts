import { OneOutsPage } from './app.po';

describe('one-outs App', () => {
  let page: OneOutsPage;

  beforeEach(() => {
    page = new OneOutsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
