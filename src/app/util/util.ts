﻿import { Headers } from "@angular/http";

export class Util {

    private static readonly _baseUrl: string = 'https://jsonplaceholder.typicode.com/';
    
    static getUrl(endpoint: string): string {
        return this._baseUrl + endpoint;
    }

    static getOptions() {
        let headers = new Headers({'Content-Type': 'application/json'});
        return {
            header: headers,
        };
    }
}
