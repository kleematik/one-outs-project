import { Component } from '@angular/core';

@Component({
  selector: 'oo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'One outs project works!';
}
