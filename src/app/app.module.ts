﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

// Custom Modules
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { AppRoutingModule } from "./app.routing.module";
import { AlbumsModule } from './albums/albums.module';
import { PhotosModule } from './photos/photos.module';
import { UsersModule } from './users/users.module';
import { TodosModule } from './todos/todos.module';
import { MiscModule } from './misc/misc.module';
import { StockInventoryModule } from './angular-advanced/reactives-forms/stock-inventory/stock-inventory.module';
// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';


@NgModule({
  imports: [
      BrowserModule,
      HttpModule,
      AngularFontAwesomeModule,
      AlbumsModule,
      PhotosModule,
      UsersModule,
      TodosModule,
      MiscModule,
      StockInventoryModule,
      AppRoutingModule

  ],
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    NotFoundComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
