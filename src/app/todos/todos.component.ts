import { Component, OnInit, Input } from '@angular/core';

// Observables
import { Observable } from 'rxjs/Observable';

// Model
import { Todo } from './../model/todo';
import { Itodo } from './../model/itodo';
import { User } from './../model/user';

// Services
import { TodoService } from './../services/todos/todo.service';
import { UsersService } from './../services/users/users.service';

@Component({
  selector: 'oo-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  title: string = 'Todos page';
  
  todosLoading;
  
  @Input('todos')
  todos: Todo[] = [];

  @Input('users')
  users: Observable<User[]>;


  constructor(
    private _usersService: UsersService,
    private _todoService: TodoService
  ) {}

  ngOnInit() {
  }

}
