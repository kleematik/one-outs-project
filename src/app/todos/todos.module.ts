import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodosRoutingModule } from "./todos.routing.module";

// Components
import { TodosComponent } from "./todos.component";
import { TodosWrapperComponent } from "./todos-wrapper/todos-wrapper.component";

import { TodoService } from "./../services/todos/todo.service";


@NgModule({
  imports: [
    CommonModule,
    TodosRoutingModule
  ],
  declarations: [
    TodosComponent,
    TodosWrapperComponent
  ],
  exports: [
    TodosWrapperComponent
  ],
  providers: [
    TodoService
  ]
})
export class TodosModule { }
