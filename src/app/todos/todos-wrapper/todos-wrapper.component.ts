import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Observables
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/takeWhile';

// Components
import { TodosComponent } from "./../todos.component";

// Data Model
import { User } from './../../model/user'
import { Todo } from './../../model/todo'
import { Itodo } from './../../model/itodo'

// Services
import { UsersService } from './../../services/users/users.service';
import { TodoService } from "./../../services/todos/todo.service";

@Component({
  selector: 'oo-todos-wrapper',
  templateUrl: './todos-wrapper.component.html',
  styleUrls: ['./todos-wrapper.component.css']
})
export class TodosWrapperComponent implements OnInit, OnDestroy {

  title: string = 'Todos page';
  todosLoading;
  
  todos: Observable<Todo[]>;
  users$: Observable<User[]>;

  tasksUndone: Todo[];
  tasksDone: Todo[];
  private _isAlive: boolean = true;


  constructor(
    private _usersService: UsersService,
    private _todoService: TodoService,
    private _activatedRoute: ActivatedRoute,
    private _routerService: Router
  ) { }

  ngOnInit() {
    this.loadUsers();
    this.loadTodosList();
    
    this._activatedRoute.url
        .subscribe(s => console.log('url=>', s));

    this._activatedRoute.params
        .subscribe(p => console.log('params=>', p));

   console.log('router state ngInit: ', this._routerService.routerState);
  }

  ngOnDestroy() {
    /**
     * How to Unsubscribe Observable in Angular ?
     * 
     * Here are two common approaches:
     * 
     *  1. Store the subscription and invoke the unsubscribe() method when the component is destroyed.
     *  2. Use the takeWhile() operator to trigger the unsubscription.
     *
     */
    console.log('TodosWrapperComponent is Destroyed');
    console.log('router state ngDestroy: ', this._routerService.routerState);
    this._isAlive = false;

  }

  reloadTodosList(filter) {
    this.loadTodosList(filter);
  }

  updateRoute() {
    this._activatedRoute.queryParams
        .subscribe(q => console.log('queryParams=>', q));

    this._activatedRoute.fragment
        .subscribe(f => console.log('fragment=>', f));
  }

  private loadUsers() {
   this._usersService.getUsers()
        .subscribe ( users => this.users$ = Observable.of(users) ); 
  }

  private loadTodosList(filter?) {
    this.todosLoading = true;
    
    this._todoService.getTodos(filter)
        .map( todos => this.format(todos) )
        .takeWhile( () => {
          console.log('takeWhile() : Mirroring the source observable');
          return this._isAlive;
        })
        .subscribe( todos => {
          this.tasksUndone = todos.filter( todo => !todo.completed);
          this.tasksDone = todos.filter( todo => todo.completed);
        },
        error => console.log(error),
        () => {
          console.log('subscribe\'s complete method is called');
          this.todosLoading = false;
        });
  }

  private format(todos: Itodo[]): Todo[] {
    
    return todos.map( t => {
      let todo: Todo = new Todo();
      
      todo.id = t.id;
      todo.title = t.title;
      todo.completed = t.completed;

      return todo;
    });
  }
}
