import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';    

// Components
import { TodosWrapperComponent } from "./todos-wrapper/todos-wrapper.component";


const routes: Route[] = [
    { 
      path: 'todos',
      children: [
        {
          path: '',
          pathMatch: 'full',
          component: TodosWrapperComponent
        }
      ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TodosRoutingModule {}