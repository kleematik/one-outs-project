﻿import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { UsersRoutingModule } from './users/users.routing.module';

// Components
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';


const routes: Route[] = [
    { 
        path: '',
        pathMatch: 'full',
        component: HomeComponent 
    },
    { 
        path: '**', 
        component: NotFoundComponent
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}