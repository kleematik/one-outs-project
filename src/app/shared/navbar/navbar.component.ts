import { Component, OnInit } from '@angular/core';


interface Nav {
  link: string,
  label: string,
}

@Component({
  selector: 'oo-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  navs: Nav[] = [
    {
      link: "users",
      label:"Users"
    },
    {
      link: "albums",
      label:"Albums"
    },
    {
      link: "photos",
      label:"Photos"
    },
    {
      link: "todos",
      label:"Todos"
    },
    {
      link: "misc",
      label: "Miscalleneous"
    },
  ];

  dropdownNavs: Nav[] = [
    {
      link: "advanced-components",
      label: "Angular Components (advanced)",
    },
    {
      link: "directives",
      label: "Angular Directives",
    },
    {
      link: "pipes",
      label: "Angular Pipes",
    },
    {
      link: "reactive-forms",
      label: "Angular Reactive Forms",
    },
    {
      link: "routing",
      label: "Angular Routing (advanced)",
    },
    {
      link: "unit-testing",
      label: "Angular Unit testing",
    },
    {
      link: "dependency-injection-and-zones",
      label: "Angular Dependency Injection and Zones",
    },
    {
      link: "state-management-with-rx",
      label: "Angular State management",
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
