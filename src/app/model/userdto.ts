export interface UserDto {
    id: number;
    name: string;
    email: string;
    username: string;
    address;
    phone: string;
    website: string;
    company;
}