﻿import { Dtobject } from "./dtobject";
import { Address } from "./address";
import { Company } from "./Company";


export class User implements Dtobject {
    private _id: number;
    private _name: string;
    private _email: string;
    private _username: string;
    private _address: Address;
    private _phone: string;
    private _website: string;
    private _company: Company;

    constructor() {
        this._address = new Address();
        this._company = new Company();
    }

    get id() {
        return this._id;
    }

    set id (value: number) {
        this._id = value;
    }

    get username() {
        return this._username;
    }
    
    set username(value: string) {
        this._username = value;
    }

    get name() {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get email() {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get website() {
        return this._website;
    }

    set website(value: string) {
        this._website = value;
    }

    get phone(): string {
        return this._phone;
    }

    set phone(value: string) {
        this._phone = value;
    }

    get address(): Address {
        return this._address;
    }

    set address(value: Address) {
        this._address = value;
    }

    get company(): Company {
        return this._company;
    }

    set company(value: Company) {
        this._company = value;
    }

}
