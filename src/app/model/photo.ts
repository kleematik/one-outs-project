import { Dtobject } from "./dtobject";

export class Photo implements Dtobject {
    
    private _id: number;
    private _title: string;
    private _thumbnailUrl: string;
    private _url: string;

    constructor(){}

    get id() {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get title() {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get thumbnailUrl() {
        return this._thumbnailUrl;
    }

    setthumbnailUrlid(value: string) {
        this._thumbnailUrl = value;
    }

    get url() {
        return this._url;
    }

    set url(value: string) {
        this._url = value;
    }
}
