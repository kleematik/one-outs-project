﻿export class Company {
    private _name: string;
    private _slogan: string;

    constructor() {}

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get slogan(): string {
        return this._slogan;
    }

    set slogan(value: string) {
        this._slogan = value;
    }
}