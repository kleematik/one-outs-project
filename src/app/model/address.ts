﻿import { Geo } from "./geo";

export class Address {
    private _street: string;
    private _suite: string;
    private _city: string;
    private _zipcode: string;
    private _geo: Geo;

    constructor() {
        this._geo = new Geo();
    }

    get street() {
        return this._street;
    }

    get suite() {
        return this._suite;
    }
    
    get city() {
        return this._city;
    }
    
    get zipcode() {
        return this._zipcode;
    }

    get geo(): Geo {
        return this._geo;
    }

    set street(value: string) {
        this._street = value;
    }

    set suite(value: string) {
        this._suite = value;
    }

    set city(value: string) {
        this._city = value;
    }

    set zipcode(value: string) {
        this._zipcode = value;
    }

    set geo(value: Geo) {
        this._geo = value;
    }

}