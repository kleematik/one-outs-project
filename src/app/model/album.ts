import { Dtobject } from "./dtobject";

export class Album implements Dtobject {

    private _id: number;
    private _title: string;

    constructor() {}

    get id() {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get title() {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }
}
