export class Todo {

    private _id: number;
    private _title: string;
    private _completed: boolean;

    constructor() {}

    get id() {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get title() {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get completed() {
        return this._completed;
    }

    set completed(value: boolean) {
        this._completed = value;
    }
}
