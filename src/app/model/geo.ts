﻿export class Geo {
    private _lat: string;
    private _long: string;

    constructor() {}

    get lat(): string {
        return this._lat;
    }

    get long(): string {
        return this._long;
    }

    set lat(value: string) {
        this._lat = value;
    }

    set long(value: string) {
        this._long = value;
    }
}