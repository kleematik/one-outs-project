import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';


import { MiscComponent } from './misc.component';

const routes: Route[] = [
  {
    path: 'misc',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MiscComponent
      },
      
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class MiscRoutingModule {}