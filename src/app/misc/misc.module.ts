import { NgModule } from '@angular/core';

import { MiscRoutingModule } from './misc-routing.module';

import { MiscComponent } from './misc.component';
import { CreditCardDirective } from '../directives/credit-card/credit-card.directive';
import { TooltipDirective } from '../directives/tooltip/tooltip.directive';

@NgModule({
  declarations: [
    MiscComponent,
    CreditCardDirective,
    TooltipDirective,
  ],
  imports: [
    MiscRoutingModule
  ]

})
export class MiscModule {}



