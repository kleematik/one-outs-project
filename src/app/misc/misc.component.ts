import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'oo-misc',
  templateUrl: './misc.component.html',
  styleUrls: ['./misc.component.css']
})
export class MiscComponent implements OnInit {

  title: string = 'Angular pro learning';
  constructor() { }

  ngOnInit() {
  }

}
