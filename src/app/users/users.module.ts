﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users.routing.module';

import { UsersComponent } from './../users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserFormComponent } from './user-form/user-form.component';

import { UsersService } from './../services/users/users.service';

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      UsersRoutingModule
  ],
  declarations: [
      UsersComponent,
      UserDetailComponent,
      UserFormComponent
  ],
  exports: [
      UsersComponent,
      UserDetailComponent
  ],
  providers: [
      UsersService
  ]
})
export class UsersModule { }
