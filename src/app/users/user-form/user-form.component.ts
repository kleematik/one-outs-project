import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { User } from '../../model/user';

@Component({
  selector: 'oo-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  private _detail: User;
  private _modeEdit: boolean = false;

  @Output('edit')
  edit: EventEmitter<any> = new EventEmitter();

  @Output('back')
  back: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  OnNameChange(value: string) {
    this.detail.name = value;
  }

  OnEmailChange(value: string) {
    this.detail.email = value;
  }

  OnPhoneChange(value: string) {
    this.detail.phone = value;
  }

  OnCompanyNameChange(value: string) {
    this.detail.company.name = value;
  }

  OnSubmitForm(data: any, isValid) {
    if (isValid) {
      this.edit.emit(data);
    }
    this.toggleEdit();
  }

  toggleEdit() {
    this.modeEdit = !this.modeEdit;
  }

  OnBack() {
    this.back.emit()
  }

  get modeEdit() {
    return this._modeEdit;
  }

  @Input('modeEdit')
  set modeEdit(value) {
    this._modeEdit = value;
  }

  @Input('detail')
  set detail(value) {
    this._detail = value;
  }

  get detail() {
    return this._detail;
  }

}
