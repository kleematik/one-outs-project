import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

// Components
//import { UserFormComponent } from '../user-form/user-form.component';

// Model
import { User } from "./../../model/user";
import { UserDto } from "./../../model/userdto";

// Services
import { UsersService } from "./../../services/users/users.service";


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {

  title: string = 'User details page';
  user: User = new User();
  userLoading = true;
  modeEdit = false;
  
  constructor(
    private _usersService: UsersService,
    private _router: ActivatedRoute,
    private _location: Location,
    private _routerService: Router
  ) { }

  ngOnInit() {
    this.loadUser();
    console.log('router state ngOnInit: ', this._routerService.routerState);
  }

  ngOnDestroy() {
    console.log('router state ngOnDestroy: ', this._routerService.routerState);
  }

  private loadUser(): void {
    this._router.params
        .switchMap( (params:Params) => this._usersService.getUser( parseInt(params['id']) ) )
        .subscribe( user => { 
          this.user = this.format(user);
          this.userLoading = false;
        });
  }

  backButton(): void {
    this._location.back();
  }

  OnEdit(event) {
   //console.log('event =>', event);
   this._usersService.update(event)
        .subscribe( user => {
          console.log('Server response => ', user);
          this.user = this.format(user);
          return this.user;
        })
  }
  
  private format(user: UserDto): User {
    let u = new User();

    u.id = user.id;
    u.name = user.name;
    u.username = user.username;
    u.email = user.email;
    u.phone = user.phone;

    u.address.city = user.address.city;
    u.address.suite = user.address.suite;
    u.address.zipcode = user.address.zipcode;
    u.address.street = user.address.street;
    u.address.geo.lat = user.address.geo.lat;
    u.address.geo.long = user.address.geo.lng;

    u.company.name = user.company.name;
    u.company.slogan = user.company.catchPhrase;

    return u;
  }

}
