import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';


// Components
import { UsersComponent } from './users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';


const routes: Route[] = [
  {
    path: 'users',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: UsersComponent
      },
      {
        path: ':id',
        component: UserDetailComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class UsersRoutingModule{}