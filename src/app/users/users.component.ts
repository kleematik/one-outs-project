﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

// Observables
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/takeWhile';

// Model
import { User } from './../model/user';

// Services
import { UsersService } from './../services/users/users.service';

@Component({
  selector: 'oo-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {

    title: string = 'Users page';
    users: Observable<User[]>;
    usersLoading: boolean = true;

    private _usersAlive: boolean = true;

    constructor(
      private _userService: UsersService,
      private _routerService: Router) { }

    ngOnInit() {
      this.loadUsers();
      console.log('router state ngOnInit: ', this._routerService.routerState);
    }

    ngOnDestroy() {
      console.log('UsersComponent is destroyed');
      console.log('router state ngOnDestroy: ', this._routerService.routerState);
      this._usersAlive = false;
    }

    private loadUsers(): void {
      this._userService.getUsers() 
          .takeWhile( () => this._usersAlive )
          .subscribe(
            users => this.users = Observable.of(users),
            error => console.log('error when loading users', error),
            () => this.usersLoading = false );
    }

}
