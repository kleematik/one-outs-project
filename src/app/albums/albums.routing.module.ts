import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

// Components
import { AlbumsComponent } from './albums.component';
import { AlbumDetailComponent } from './album-detail/album-detail.component';


const routes: Route[] = [
  { path: 'albums',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: AlbumsComponent
      },
      {
        path: ':id',
        component: AlbumDetailComponent
      }
    ]
  },
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AlbumsRoutingModule{}