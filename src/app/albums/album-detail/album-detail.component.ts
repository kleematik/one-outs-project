import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/switchMap';

import { Photo } from "./../../model/photo";
import { PhotosComponent } from "./../../photos/photos.component";
import { PhotosService } from "./../../services/photos/photos.service";
import { Util } from "./../../util/util";

@Component({
  selector: 'oo-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.css']
})
export class AlbumDetailComponent implements OnInit {

  title: string = 'Album details page';
  photosLoading = true;
  photos: Observable<Photo[]>;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _photosService: PhotosService
  ) { }

  ngOnInit() {
    this.loadAlbumDetails();
  }

  private loadAlbumDetails(): void {
    this._activatedRoute.params
        .switchMap( (params: Params) => this._photosService.getPhotosFromAlbum( parseInt(params['id']) ))
        .subscribe(
          photos => {
            this.photos = Observable.of(photos);
            this.photosLoading = false;
          });
  }
}
