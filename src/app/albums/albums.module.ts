﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AlbumsRoutingModule } from './albums.routing.module';
import { PhotosModule } from './../photos/photos.module';

import { AlbumsComponent } from "./albums.component";
import { AlbumService } from "./../services/album/album.service";
import { AlbumDetailComponent } from './album-detail/album-detail.component';

@NgModule({
  imports: [
      CommonModule,
      PhotosModule,
      AlbumsRoutingModule
  ],
  declarations: [
      AlbumsComponent,
      AlbumDetailComponent
  ],
  exports: [
      AlbumsComponent,
  ],
  providers: [
      AlbumService
  ]
})
export class AlbumsModule { }
