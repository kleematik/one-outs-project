﻿import { Component, OnInit, OnDestroy } from '@angular/core';

// Observables
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/takeWhile';

// Model
import { Album } from './../model/album';
import { User } from './../model/user';

// Services
import { AlbumService } from './../services/album/album.service';
import { UsersService } from './../services/users/users.service';


@Component({
  selector: 'oo-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit, OnDestroy {

  title: string = 'Albums page';
  albums: Observable<Album[]>;
  users: Observable<User[]>;
  albumsLoading;

  //private _albumsAlive = true;

  constructor(
    private _albumService: AlbumService,
    private _usersService: UsersService
    
  ) { }

  ngOnInit() {
    this.loadUsers();
    this.loadAlbums();
  }

  ngOnDestroy() {
    console.log('AlbumsComponent is destroyed');
    //this._albumsAlive = false;
  }

  private loadUsers(): void {
    this._usersService.getUsers()
        .subscribe( users => this.users = Observable.of(users) )
  }

  private loadAlbums(filter?): void {
    this.albumsLoading = true;
    this._albumService.getAlbums(filter)
        .subscribe(
          albums => this.albums = Observable.of(albums),
          error => console.log(error),
          () => this.albumsLoading = false );
  }

  reloadAlbums(filter): void {
    this.loadAlbums(filter);
  }

}
