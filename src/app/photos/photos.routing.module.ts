import { NgModule } from '@angular/core';
import { RouterModule, Route} from '@angular/router';

// Components
import { PhotosComponent } from './photos.component';

const routes: Route[] = [
    { 
      path: 'photos', 
      children: [
        {
          path: '',
          pathMatch: 'full',
          component: PhotosComponent
        }
      ]
    },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PhotosRoutingModule {}