﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Routing module
import { PhotosRoutingModule } from './photos.routing.module';

// Components
import { PhotosComponent } from "./photos.component";

// Services
import { PhotosService } from "./../services/photos/photos.service";

@NgModule({
  imports: [
    CommonModule,
    PhotosRoutingModule
  ],
  declarations: [
      PhotosComponent
  ],
  exports: [
      PhotosComponent
  ],
  providers: [
      PhotosService
  ]
})
export class PhotosModule {}
