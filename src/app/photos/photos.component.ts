﻿import { Component, Input, OnInit, OnDestroy } from '@angular/core';

// Observables
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

// Model
import { Photo } from './../model/photo';

// Services
import { PhotosService } from './../services/photos/photos.service';

@Component({
  selector: 'oo-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit, OnDestroy {

    @Input('title') 
    title: string = 'Photos page';
    
    @Input('photos') 
    photos: Observable<Photo[]>;
    
    @Input('photosLoading')
    photosLoading = true;

    constructor(private _photosService: PhotosService) { }

   ngOnInit() {
      this.loadPhotos();
   }

   ngOnDestroy() {
    console.log('PhotosComponent is destroyed');
  }

   private loadPhotos(): void {
     this._photosService.getPhotos()
        .subscribe(
          photos => this.photos = Observable.of(photos),
          error => console.log(error),
          () => this.photosLoading = false );
   }
}
