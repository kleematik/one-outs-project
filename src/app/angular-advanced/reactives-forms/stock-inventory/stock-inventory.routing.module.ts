import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


// Components
import { StockInventoryComponent } from './containers/stock-inventory.component';

const routes: Routes = [
  {
    path: 'angular-advanced',
    children: [
      {
        path: 'reactive-forms',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: StockInventoryComponent,
          },

        ],
      },

    ],
  },

];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class StockInventoryRoutingModule {}