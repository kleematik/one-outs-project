import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

// Models
import { Product } from '../../model/product.interface';

@Component({
  selector: 'oo-stock-selector',
  templateUrl: './stock-selector.component.html',
  styleUrls: ['./stock-selector.component.css']
})
export class StockSelectorComponent implements OnInit {

  @Input('parent')
  parent: FormGroup;
  
  @Input('products')
  products: Product[];

  @Output('add')
  add: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  OnAdd() {
    this.add.emit(this.selector);
  }

  get selector() {
    return this.parent.get('selector').value;
  }
}
