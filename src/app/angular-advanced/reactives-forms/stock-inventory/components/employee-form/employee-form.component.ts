import { 
  Component, 
  OnInit, 
  Input 
} from '@angular/core';
import { 
  FormGroup, 
  FormControl,
  AbstractControl
} from '@angular/forms';


@Component({
  selector: 'oo-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {

  @Input('parent')
  parent: FormGroup;

  constructor() { }

  ngOnInit() {
  }

  hasError(formControlName) {
    let control = this._formControl('employee', formControlName);
    if(control) {
      let errors = control.errors;
      
      if(errors) {
        let required = errors.required;
        let minLength = control.errors.minlength.requiredLength;

        if (required) {
          return control.touched && required;
        } else if (minLength) {
          return control.dirty && minLength;
        }
      }

    }
  }

  required(inputName) {
    let control = this._formControl('employee', inputName);
    if(control)
      return control.touched &&
          control.errors.minlength.requiredLength < 3;
  }

  minLength(inputName) {
    let control = this._formControl('employee', inputName);
    if(control)
      return control.touched &&
          control.errors.minlength.requiredLength < 3;
  }

  requiredLength(formGroupName, formControlName) {
    let control = this._formControl(formGroupName, formControlName);
    if(control)
      return control.errors.minlength.requiredLength;
  }


/**
 * Returns a FormGroup of a form model
 * @param formGroupName Name of the FormGroup
 */
  private _formGroup(formGroupName): FormGroup {
    return this.parent.controls[formGroupName] as FormGroup;
  }

/**
 * Returns a FormControl of a given FormGroup
 * @param formGroupName Name of the FormGroup
 * @param formControlName Name of the FormControl
 */
  private _formControl(formGroupName, formControlName): FormControl {
    return ( this._formGroup(formGroupName) as FormGroup ).controls[formControlName] as FormControl;
  }
}
