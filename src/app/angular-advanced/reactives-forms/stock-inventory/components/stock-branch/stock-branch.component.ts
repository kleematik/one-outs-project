import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'oo-stock-branch',
  templateUrl: './stock-branch.component.html',
  styleUrls: ['./stock-branch.component.css']
})
export class StockBranchComponent implements OnInit {

  @Input('parent')
  parent: FormGroup;
  
  constructor() { }

  ngOnInit() {
  }

}
