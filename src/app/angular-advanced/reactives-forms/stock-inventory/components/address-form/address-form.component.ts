import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'oo-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements OnInit {

  @Input('parent')
  parentForm: FormGroup;

  constructor() {
    console.log('creation instance of ', AddressFormComponent);
   }

  ngOnInit() {
  }

}
