import { 
  Component, 
  OnInit 
} from '@angular/core';
import { 
  FormArray, 
  FormGroup, 
  FormControl, 
  FormBuilder, 
  Validators 
} from '@angular/forms';


// Components
import { StockBranchComponent } from '../components/stock-branch/stock-branch.component';
import { StockSelectorComponent } from '../components/stock-selector/stock-selector.component';
import { StockProductsComponent } from '../components/stock-products/stock-products.component';

// Models 
import { Product } from '../model/product.interface';

@Component({
  selector: 'oo-stock-inventory',
  templateUrl: './stock-inventory.component.html',
  styleUrls: ['./stock-inventory.component.css']
})
export class StockInventoryComponent implements OnInit {

  title: string = 'Angular Reactive Forms';
  private _products: Product[] = [
    {id: 1, price: 700, name: "HP Pavilion"},
    {id: 2, price: 1200, name: "MacBook"},
    {id: 3, price: 900, name: "IPhone 7"},
    {id: 4, price: 600, name: "Apple Watch"},
    {id: 5, price: 400, name: "Disque Dur 2'5 pouces"},
  ];

  private _controls = {
    store: new FormGroup({
      branch: new FormControl(''),
      code: new FormControl('')
    }),
    selector: new FormGroup({
      product_id: new FormControl(''),
      quantity: new FormControl(10)
    }),
    stock: new FormArray([])
  };
  
  //stockInventoryForm = new FormGroup(this._controls);
  employeeForm: FormGroup;

  constructor(
    private _fb: FormBuilder
  ) { }

  ngOnInit() {
    this.employeeForm = this._fb.group(this.employeesControls);
  } 

  get employeesControls () {
    return {
      employee : this._fb.group({
        firstName: this._fb.control('tommy', [Validators.required, Validators.minLength(3)]),
        lastName: this._fb.control('', [Validators.required, Validators.minLength(3)])
      }),

      address: this._fb.group({
        address: this._fb.control(''),
        zipcode: this._fb.control(''),
        city: this._fb.control(''),
        state: this._fb.control('')
      })
    };
  }

  OnSubmit() {
    console.log(this.employeeForm.value);
  }

  /*get products() {
    return this._products;
  }*/

  /*addStock(event) {
    console.log('selector values =>', event);
    const control = ( this.stockInventoryForm.get('stock') as FormArray );
    control.push(this.createStock(event));
  }
  
  private createStock(stock) {
    return new FormGroup({
      product_id: new FormControl(parseInt(stock.product_id, 10) || ''),
      quantity: new FormControl(stock.quantity || 10 )
    })
  }*/
}
