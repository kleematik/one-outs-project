import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Custom routes
import { StockInventoryRoutingModule } from './stock-inventory.routing.module';


// Components
import { StockInventoryComponent } from './containers/stock-inventory.component';
import { StockBranchComponent } from './components/stock-branch/stock-branch.component';
import { StockSelectorComponent } from './components/stock-selector/stock-selector.component';
import { StockProductsComponent } from './components/stock-products/stock-products.component';
import { AddressFormComponent } from './components/address-form/address-form.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StockInventoryRoutingModule,
  ],
  declarations: [
    StockInventoryComponent,
    StockBranchComponent,
    StockSelectorComponent,
    StockProductsComponent,
    AddressFormComponent,
    EmployeeFormComponent,
  ],
  exports: [
    StockInventoryComponent,
  ]
})
export class StockInventoryModule { }
