import { Directive, Input, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[tooltip]',
  exportAs: 'tooltip'
})
export class TooltipDirective implements OnInit {

  toolTipDivElt = document.createElement('div');
  visible: boolean = false;

  constructor(private _el: ElementRef) {
    console.log('Tooltip Directive instantiation');
   }

   ngOnInit() {
     this.toolTipDivElt.className = 'tooltip';
     this._el.nativeElement.appendChild(this.toolTipDivElt);    
     this._el.nativeElement.classList.add('tooltip-container');
   }

  @Input()
  set tooltip(value) {
    this.toolTipDivElt.textContent = value;
  }

  show(instance) {
    this.toolTipDivElt.classList.add('tooltip--active');
    console.log('tooltip instance', instance);
  }

  hide() {
    this.toolTipDivElt.classList.remove('tooltip--active');
  }
}
