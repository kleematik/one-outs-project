import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[credit-card]'
})
export class CreditCardDirective {

  isValid: boolean = false;

  @HostBinding('style.border')
  border: string;

  constructor() { 
    //console.log('Hello from CreditCardDirective');
  }

  @HostListener('input', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;

    let trimmed = input.value.replace(/\W+/g, '');

    if (trimmed.length >= 16) {
      trimmed = trimmed.substr(0, 16);
      this.isValid = true;
    } else {
      this.isValid = false; 
    }

    //console.log('trimmed =>', trimmed);
    let numbers = [];
    for(let i = 0, len = trimmed.length; i < len; i+=4) {
      numbers.push(trimmed.substr(i, 4));
    }

    input.value = numbers.join(' ');

    this.border = '';
    if ( this.isValid ) {
      this.border = '1px solid green';
    }

    if ( /[^\d+]/.test(trimmed) ) {
      this.border = '1px solid red';
    } 
  }
}
