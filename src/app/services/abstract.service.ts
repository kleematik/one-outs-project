import { Http, Response } from "@angular/http";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Base } from "./base";
import { Util } from "./../util/util";

export abstract class AbstractService<T> {

  constructor(private _http: Http) {
    //console.log('http instance injected by angular', this._http);
   }

    protected http() {
     return this._http;
    }

    protected getData(endpoint: string): Observable<T[]> {
      let url = Util.getUrl(endpoint);
      //console.log('endpoint', url);

      return this._http.get(url, Util.getOptions())
                  .map( (res: Response) => res.json() as T[] )
                  .catch( this.handleError );
    }

    protected handleError(error) {
      console.log('error=>', error);
      return Observable.throw(error);
    }
}
