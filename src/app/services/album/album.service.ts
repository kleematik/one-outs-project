import { Injectable } from '@angular/core';
import { Http } from "@angular/http";

import { Observable } from "rxjs/Observable";

import { Album } from "./../../model/album";
import { AbstractService } from "./../abstract.service";

@Injectable()
export class AlbumService extends AbstractService<Album> {

  constructor(_http: Http) {
    super(_http);
  }

  getAlbums(filter?): Observable<Album[]> {
    let endpoint = filter && filter.userId ? `albums?userId=${filter.userId}` : 'albums';
    return super.getData(endpoint);
  }
}
