import { Observable } from "rxjs/Observable";

export interface Base<T> {
    getData(endpoint: string): Observable<T[]>;
}
