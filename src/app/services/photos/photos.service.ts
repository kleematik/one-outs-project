import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// Observables
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Photo } from "./../../model/photo";
import { Album } from "./../../model/album";
import { AbstractService } from "./../abstract.service";
import { Util } from "./../../util/util";

@Injectable()
export class PhotosService extends AbstractService<Photo> {

  constructor(_http: Http) {
    super(_http);
   }

   getPhotos(): Observable<Photo[]> {
     return super.getData('photos');
   }

   getPhotosFromAlbum(albumId: number): Observable<Photo[]> {
      let url = `${Util.getUrl('photos')}?albumId=${albumId}`;
      
      return super.http().get(url, Util.getOptions)
                          .map( res => res.json() as Photo[] )
                          .catch( super.handleError );
   }


}
