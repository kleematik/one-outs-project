﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

// Observables
import { Observable } from 'rxjs/Observable';

import { UserDto } from "./../../model/userdto";
import { User } from "./../../model/user";
import { AbstractService } from "./../abstract.service";
import { Util } from "./../../util/util";

@Injectable()
export class UsersService extends AbstractService<User> {

    constructor(_http: Http) {
        super(_http);
    }

    getUsers(): Observable<User[]> {
        return super.getData('users');
    }

    getUser(userId: number): Observable<UserDto> {
        let url = `${Util.getUrl('users')}/${userId}`;
        return super.http().get(url, Util.getOptions())
            .map(res => res.json())
            .catch(super.handleError);
    }

    update(user) {
        let url = `${Util.getUrl('users')}/${user.userid}`;
        let data = this.format(user);
        console.log('data =>', data);

        return super.http().put(url, data, Util.getOptions)
            .map((res: Response) => res.json())
            .catch( super.handleError );
    }

    private format(user) {
        console.log('user=>', user);
        return {
            "id" : user.userid,
            "name" : user.username,
            //"username" : user.username,
            "email" : user.email,
            "phone" : user.phone,
            /*"address" : {
                "city" : user.address.city,
                "suite" : user.address.suite,
                "zipcode" : user.address.zipcode,
                "street" : user.address.street,
                "geo" : {
                    "lat": user.address.geo.lat,
                    "lng": user.address.geo.long,
                }
            },*/
            "company" : {
                "name": user.companyName,
                //"catchPhrase": user.company.slogan
            }
        };
    }

}
