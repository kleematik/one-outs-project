import { Injectable } from '@angular/core';
import { Http } from "@angular/http";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

import { AbstractService } from "./../abstract.service";
import { Itodo } from "./../../model/itodo";
import { Todo } from "./../../model/todo";

@Injectable()
export class TodoService extends AbstractService<Todo> {

  constructor (http: Http) {
    super(http);
   }

   getTodos(filter?): Observable<Itodo[]> {
     let endpoint = filter && filter.userId ? `todos?userId=${filter.userId}` : 'todos';
     return super.getData(endpoint);
   }

}
